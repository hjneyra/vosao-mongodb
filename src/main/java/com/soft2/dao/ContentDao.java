package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.ContentEntity;

@Repository
public class ContentDao extends AbstractDao<ContentEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;

	public ContentDao() {
		super();
	}
	
	public List<ContentEntity> select(final String parentClass, 
			final Long parentKey) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parentClass").is(parentClass));
		query.addCriteria(Criteria.where("parentKey").is(parentKey));
		return mongoTemplate.find(query, ContentEntity.class);
	}
	
	public ContentEntity getByLanguage(final String parentClass, 
			final String pageId, final String language) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parentClass").is(parentClass));
		query.addCriteria(Criteria.where("parentKey").is(pageId));
		query.addCriteria(Criteria.where("languageCode").is(language));
		return mongoTemplate.findOne(query, ContentEntity.class);
	}

	public void removeById(String className, String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parentClass").is(className));
		query.addCriteria(Criteria.where("parentKey").is(id));
		mongoTemplate.remove(query, ContentEntity.class);
	}
	
	@Override
	protected Class<ContentEntity> getCollectionClass() {
		return ContentEntity.class;
	}

}
