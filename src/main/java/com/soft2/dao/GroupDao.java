package com.soft2.dao;

import org.springframework.stereotype.Repository;

import com.soft2.entity.GroupEntity;

@Repository
public class GroupDao extends AbstractDao<GroupEntity> {

	@Override
	protected Class<GroupEntity> getCollectionClass() {
		return GroupEntity.class;
	}

	public GroupEntity getGuestsGroup() {
		// TODO Auto-generated method stub
		return new GroupEntity();
	}

}
