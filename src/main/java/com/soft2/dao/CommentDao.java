package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.soft2.entity.CommentEntity;

@Repository
public class CommentDao extends AbstractDao<CommentEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;

	public CommentEntity save(CommentEntity obj) {
		mongoTemplate.save(obj);
		return obj;
	}
	
    public CommentDao(){
        super();
    }

	public List<CommentEntity> getByPage(String pageUrl) {
		Query query = new Query();
		query.addCriteria(Criteria.where("pageUrl").is(pageUrl));
		return mongoTemplate.find(query, CommentEntity.class);
	}
	
	public void disable(List<String> ids) {
		Query query;
		for (String id : ids) {
			query = new Query();
			query.addCriteria(Criteria.where("id").is(id));
			mongoTemplate.updateFirst(
				query, Update.update("disabled", true), CommentEntity.class);
		}
	}

	public void enable(List<String> ids) {
		Query query;
		for (String id : ids) {
			query = new Query();
			query.addCriteria(Criteria.where("id").is(id));
			mongoTemplate.updateFirst(
				query, Update.update("disabled", false), CommentEntity.class);
		}
	}

	public List<CommentEntity> getByPage(String pageUrl, boolean disabled) {
		Query query = new Query();
		query.addCriteria(Criteria.where("pageUrl").is(pageUrl).andOperator(
			Criteria.where("disabled").is(disabled)));
		query.with(new Sort(Sort.Direction.ASC, "publishDate"));
        // logger.info("Sorting by publishDate in Asc mode");
		return mongoTemplate.find(query, CommentEntity.class);
	}

	public List<CommentEntity> getByPage(String pageUrl, boolean disabled, String ascdesc) {
		Sort order = new Sort(Sort.Direction.ASC, "publishDate");
		if (ascdesc.equalsIgnoreCase("DESC")) {
			order = new Sort(Sort.Direction.DESC, "publishDate");
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("pageUrl").is(pageUrl).andOperator(
			Criteria.where("disabled").is(disabled)));
		query.with(order);
		return mongoTemplate.find(query, CommentEntity.class);
	}
	
	public void removeByPage(String url) {
        Query query = new Query();
    	query.addCriteria(Criteria.where("pageUrl").is(url));
        mongoTemplate.remove(query, CommentEntity.class);
	}

	public List<CommentEntity> getRecent(int limit) {
	    Query query = new Query();
	    query.addCriteria(Criteria.where("disabled").is(false));
	    query.with(new Sort(Sort.Direction.DESC, "publishDate"));
	    query.limit(limit);
	    return mongoTemplate.find(query, CommentEntity.class);
	}

	@Override
	protected Class<CommentEntity> getCollectionClass() {
		return CommentEntity.class;
	}
}