package com.soft2.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.soft2.entity.PageTagEntity;
import com.soft2.entity.TagEntity;

@Repository
public class PageTagDao extends AbstractDao<PageTagEntity> {

	@Override
	protected Class<PageTagEntity> getCollectionClass() {
		return PageTagEntity.class;
	}

	public List<TagEntity> selectByParent(String id) {
		// TODO needs to be implemented
		return null;
	}

	public PageTagEntity getByURL(String pageURL) {
		// TODO needs to be implemented
		return null;
	}
}
