package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.DBCollection;
import com.soft2.entity.BaseEntity;

public abstract class AbstractDao<T extends BaseEntity> {
	@Inject
	MongoTemplate mongoTemplate;


    /**
     * @return the dbCollectionl
     */
    public DBCollection getDbCollection() {
        return mongoTemplate.getCollection(getCollectionName());
    }

    
    public  List<T> getAll(){
    	return mongoTemplate.findAll(getCollectionClass());
    }
    
    public T insert(T dbObject){
        mongoTemplate.insert(dbObject);
        return dbObject;
    }
    
    public T save(T entity) {
    	mongoTemplate.save(entity);
    	return entity;
    }

    public void remove(T dbObject){
        mongoTemplate.remove(dbObject);
    }
    
    public void remove(List<String> ids){
    	for (String id : ids) {
    		removeById(id);
    	}
    }

    public void update(Query query, Update update){
        mongoTemplate.updateFirst(query, update, getCollectionClass());
    }


	public void removeById(String id) {
		remove(getById(id));
	}

    public T getById(String id){
    	Query query = new Query();
    	query.addCriteria(Criteria.where("id").is(id));
    	return mongoTemplate.findOne(query, getCollectionClass());
    }
    
    protected String getCollectionName() {
    	return getCollectionClass().getSimpleName();
    }

    protected abstract Class<T> getCollectionClass();
}