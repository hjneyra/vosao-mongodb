package com.soft2.dao;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.LanguageEntity;

@Repository
public class LanguageDao extends AbstractDao<LanguageEntity> {

	@Inject
	MongoTemplate mongoTemplate;
	
	public LanguageEntity save(LanguageEntity obj) {
		mongoTemplate.save(obj);
		return obj;
	}
	
	public LanguageDao() {
		super();
	}

	public LanguageEntity getByCode(String code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("code").is(code));
		return mongoTemplate.findOne(query, LanguageEntity.class);
	}

	@Override
	protected Class<LanguageEntity> getCollectionClass() {
		return LanguageEntity.class;
	}
}
