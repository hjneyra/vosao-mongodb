package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.soft2.entity.ConfigEntity;
import com.soft2.service.VosaoContextService;

@Repository
public class ConfigDao extends AbstractDao<ConfigEntity> {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	MongoTemplate mongoTemplate;
	
	public ConfigDao() {
		super();
	}

	public ConfigEntity getConfig() {
		List<ConfigEntity> list = getAll();
		if (list.size() > 0) {
			return list.get(0);
		}
		logger.error("Config for site was not found!");
		return new ConfigEntity();
	}

	public ConfigEntity save(ConfigEntity obj) {
		VosaoContextService.getInstance().setConfig(obj);
		mongoTemplate.save(obj);
		return obj;
	}
	
	@Override
	protected Class<ConfigEntity> getCollectionClass() {
		return ConfigEntity.class;
	}

}
