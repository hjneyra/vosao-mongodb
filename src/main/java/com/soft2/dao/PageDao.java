package com.soft2.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.PageEntity;
import com.soft2.entity.ContentEntity;
import com.soft2.entity.helper.PageHelper;

@Repository
public class PageDao extends AbstractDao<PageEntity> {

	@Inject
	MongoTemplate mongoTemplate;
	
	@Inject
	ContentDao contentDao;
	
	@Inject
	CommentDao commentDao;
	
	private static final String PAGE_CLASS_NAME = PageEntity.class.getName();

	public PageDao() {
		super();
	}
	
	public void remove(String id) {
		PageEntity page = getById(id);
		if (page != null) {
			List<PageEntity> versions = selectByUrl(page.getFriendlyURL());
			for (PageEntity version : versions) {
				removeVersion(version.getId());
			}
			List<PageEntity> children = selectAllChildren(page.getFriendlyURL());
			for (PageEntity child : children) {
				remove(child.getId());
			}
			commentDao.removeByPage(page.getFriendlyURL());
		}
	}
	
	public void removeVersion(String id) {
		PageEntity page = getById(id);
		if (page != null) {
			contentDao.removeById(PAGE_CLASS_NAME, id);
			remove(id);
		}
	}

	public List<PageEntity> selectAllChildren(final String parentUrl) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parentUrl").is(parentUrl));
		return mongoTemplate.find(query, PageEntity.class);
	}
	
	public List<PageEntity> selectAllChildren(final String parentUrl,
			Date startDate, Date endDate) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parentUrl").is(parentUrl));
		query.addCriteria(Criteria.where("publishDate").gte(startDate));
		query.addCriteria(Criteria.where("publishDate").lt(startDate));
		return mongoTemplate.find(query, PageEntity.class);
	}

	public void remove(List<String> ids) {
		for (String id : ids) {
			remove(id);
		}
	}

	public List<PageEntity> getByParent(final String url) {
		List<PageEntity> result = filterLatestVersion(selectAllChildren(url));
		Collections.sort(result, PageHelper.PUBLISH_DATE);
		return result;
	}

	public PageEntity getByUrl(final String url) {
		List<PageEntity> result = filterApproved(selectByUrl(url));
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	public String getContent(String pageId, String languageCode) {
		ContentEntity content = contentDao.getByLanguage(
				PAGE_CLASS_NAME, pageId, languageCode);
		if (content != null) {
			return content.getContent();
		}
		return null;
	}

	public ContentEntity setContent(String pageId, String languageCode, 
				String content) {
		ContentEntity contentEntity = contentDao.getByLanguage(
				PAGE_CLASS_NAME, pageId, languageCode);
		if (contentEntity == null) {
			contentEntity = new ContentEntity(PAGE_CLASS_NAME, pageId, 
					languageCode, content);
		}
		else {
			contentEntity.setContent(content);
		}
		return contentDao.insert(contentEntity);
	}

	public List<ContentEntity> getContents(Long pageId) {
		return contentDao.select(PAGE_CLASS_NAME, pageId);
	}
	
	private List<PageEntity> filterLatestVersion(List<PageEntity> list) {
		Map<String, PageEntity> pages = new HashMap<String, PageEntity>();
		for (PageEntity page : list) {
			String key = page.getFriendlyURL();
			if (!pages.containsKey(key)
					|| pages.get(key).getVersion() < page.getVersion()) {
				pages.put(key, page);
			}
		}
		List<PageEntity> result = new ArrayList<PageEntity>();
		result.addAll(pages.values());
		return result;
	}
	
	private List<PageEntity> filterApproved(List<PageEntity> list) {
		Map<String, PageEntity> pages = new HashMap<String, PageEntity>();
		for (PageEntity page : list) {
			if (page.isApproved() && !page.isForInternalUse()
					&& page.isPublished()) {
				String key = page.getFriendlyURL();
				if (!pages.containsKey(key)
					|| pages.get(key).getVersion() < page.getVersion()) {
					pages.put(key, page);
				}
			}
		}
		List<PageEntity> result = new ArrayList<PageEntity>();
		result.addAll(pages.values());
		return result;
	}
	
	public List<PageEntity> selectByUrl(final String url) {
		Query query = new Query();
		query.addCriteria(Criteria.where("friendlyUrl").is(url));
		List<PageEntity> result = mongoTemplate.find(query, PageEntity.class);
		Collections.sort(result, PageHelper.VERSION_ASC);
		return result;
	}
	
	public PageEntity getByUrlVersion(final String url, final Integer version) {
		Query query = new Query();
		query.addCriteria(Criteria.where("friendlyUrl").is(url));
		query.addCriteria(Criteria.where("version").is(version));
		return mongoTemplate.findOne(query, PageEntity.class);
	}
	
	public List<PageEntity> getByParentApproved(final String url) {
		List<PageEntity> result = filterApproved(selectAllChildren(url));
		Collections.sort(result, PageHelper.PUBLISH_DATE);
		return result;
	}

	public List<PageEntity> getByParentApproved(final String url, 
			Date startDate, Date endDate) {
		List<PageEntity> result = filterApproved(selectAllChildren(url,
				startDate, endDate));
		Collections.sort(result, PageHelper.PUBLISH_DATE);
		return result;
	}

	public List<PageEntity> selectByTemplate(String templateId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("templateId").is(templateId));
		return mongoTemplate.find(query, PageEntity.class);
	}

	public List<PageEntity> selectByStructure(String structureId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("structureId").is(structureId));
		return mongoTemplate.find(query, PageEntity.class);
	}

	public List<PageEntity> selectByStructureTemplate(
			String structureTemplateId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("structureTemplateId").is(
			structureTemplateId));
		return mongoTemplate.find(query, PageEntity.class);
	}


//	public List<PageEntity> getCurrentHourPublishedPages() {
//		Date endDate = new Date();
//		Date startDate = DateUtils.addHours(endDate, -1);
//		Query q = newQuery();
//		q.addFilter("publishDate", FilterOperator.GREATER_THAN_OR_EQUAL, 
//				startDate);
//		q.addFilter("publishDate", FilterOperator.LESS_THAN_OR_EQUAL, 
//				endDate);
//		return select(q, "getCurrentHourPublishedPages", 
//				params(startDate, endDate));
//	}
//	
//	public List<PageEntity> getCurrentHourUnpublishedPages() {
//		Date endDate = new Date();
//		Date startDate = DateUtils.addHours(endDate, -1);
//		Query q = newQuery();
//		q.addFilter("endPublishDate", FilterOperator.GREATER_THAN_OR_EQUAL, 
//				startDate);
//		q.addFilter("endPublishDate", FilterOperator.LESS_THAN_OR_EQUAL, 
//				endDate);
//		return select(q, "getCurrentHourUnpublishedPages", 
//				params(startDate, endDate));
//	}
	
	@Override
	protected Class<PageEntity> getCollectionClass() {
		return PageEntity.class;
	}
	
}
