package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.UserGroupEntity;

@Repository
public class UserGroupDao extends AbstractDao<UserGroupEntity> {

	@Inject
	MongoTemplate mongoTemplate;
	
	public UserGroupDao() {
		super();
	}
	
	@Override
	protected Class<UserGroupEntity> getCollectionClass() {
		return UserGroupEntity.class;
	}

	public List<UserGroupEntity> selectByUser(String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, UserGroupEntity.class);
	}

	public List<UserGroupEntity> selectByGroup(String groupId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("groupId").is(groupId));
		return mongoTemplate.find(query, UserGroupEntity.class);
	}

	public UserGroupEntity getByUserGroup(String groupId, String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId));
		query.addCriteria(Criteria.where("groupId").is(groupId));
		return mongoTemplate.findOne(query, UserGroupEntity.class);
	}

	public void removeByGroup(List<String> groupIds) {
		for (String groupId : groupIds) {
			List<UserGroupEntity> list = selectByGroup(groupId);
		    remove(getIds(list));	
		}
	}

	private List<String> getIds(List<UserGroupEntity> list) {
		List<String> result = new ArrayList<String>();
		for (UserGroupEntity e : list) {
			result.add(e.getId());
		}
		return result;
	}
	
	public void removeByUser(List<String> userIds) {
		for (String userId : userIds) {
			List<UserGroupEntity> list = selectByUser(userId);
		    remove(getIds(list));
		}
	}
	
}
