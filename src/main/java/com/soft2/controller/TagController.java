package com.soft2.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soft2.entity.TagEntity;
import com.soft2.service.TagService;
import com.soft2.service.decorator.TreeItemDecorator;

@RestController
public class TagController {
	
	@Inject
	TagService service;
	
	@RequestMapping(value = "tag", method = RequestMethod.GET)
	public List<TreeItemDecorator<TagEntity>> listTags() {
		return service.getTree();
	}

	@RequestMapping(value = "/tag/", method = RequestMethod.POST)
	public String saveTag(@RequestBody TagEntity tag) {
		return service.insert(tag);
	}
	
	@RequestMapping(value = "/tag/", method = RequestMethod.DELETE)
	public String remove(@RequestBody String id) {
		service.remove(id);
		return "Removed";
	}
	
	@RequestMapping(value = "/tag/getByPath/", method = RequestMethod.GET)
	public String getByPath(@RequestBody String id) {
		return service.getByPath(id).getId();
	}
	
	@RequestMapping(value = "/tag/getPath/", method = RequestMethod.GET)
	public String getPath(@RequestBody TagEntity tag) {
		return service.getPath(tag);
	}
	
	@RequestMapping(value = "/tag/addTag/", method = RequestMethod.POST)
	public String addTag(@RequestBody String url, @RequestBody TagEntity tag) {
		service.addTag(url, tag);
		return "Added";
	}
	
	@RequestMapping(value = "/tag/removeTag/", method = RequestMethod.DELETE)
	public String removeTag(@RequestBody String url, @RequestBody TagEntity tag) {
		service.removeTag(url, tag);
		return "Removed";
	}
	
	@RequestMapping(value = "/tag/getTree/", method = RequestMethod.GET)
	public TreeItemDecorator<TagEntity> listTags(@RequestBody String name) {
		return service.getTree(name);
	}

}
