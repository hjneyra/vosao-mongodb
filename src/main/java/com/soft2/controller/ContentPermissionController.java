package com.soft2.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soft2.entity.ContentPermissionEntity;
import com.soft2.entity.UserEntity;
import com.soft2.service.ContentPermissionService;


@RestController
public class ContentPermissionController {

	@Inject
	ContentPermissionService service;
	
	@RequestMapping(value = "/contentPermission/", method = RequestMethod.GET)
	public List<ContentPermissionEntity> listAll() {
		return service.getAll();
	}
	
	@RequestMapping(value = "/contentPermission/", method = RequestMethod.POST)
	public String saveTag(@RequestBody ContentPermissionEntity tag) {
		return service.insert(tag);
	}
	
	@RequestMapping(value = "/contentPermission/", method = RequestMethod.DELETE)
	public String remove(@RequestBody String id) {
		service.remove(id);
		return "Removed";
	}
	
	@RequestMapping(value = "/contentPermission/getGuestPermission/", method = RequestMethod.GET)
	public ContentPermissionEntity getGuestPermission(@RequestBody String url) {
		return service.getGuestPermission(url);
	}
	
	@RequestMapping(value = "/contentPermission/getPermission/", method = RequestMethod.GET)
	public ContentPermissionEntity getPermission(@RequestBody String url, 
			@RequestBody UserEntity usr) {
		return service.getPermission(url, usr);
	}
}
