package com.soft2.enums;

public enum ContentPermissionType {
	DENIED, READ, WRITE, PUBLISH, ADMIN;
}
