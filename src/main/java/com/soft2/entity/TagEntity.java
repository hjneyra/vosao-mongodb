package com.soft2.entity;

import java.util.ArrayList;
import java.util.List;

public class TagEntity extends BaseEntityImpl {

	private String parent;
	private String name;
	private String title;
	private List<String> pages;
	
	public TagEntity() {
		pages = new ArrayList<String>();
	}

	public TagEntity(String aParent, String aName, String aTitle) {
		this();
		name = aName;
		title = aTitle;
		parent = aParent;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public List<String> getPages() {
		return pages;
	}

	public void setPages(List<String> pages) {
		this.pages = pages;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
