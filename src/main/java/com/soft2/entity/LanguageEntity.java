package com.soft2.entity;


public class LanguageEntity extends BaseEntityImpl {

	public static final String ENGLISH_CODE = "en";
	public static final String ENGLISH_TITLE = "English";
	
	private String code;
	private String title;

	public LanguageEntity() {
		code = "";
		title = "";
	}

	public LanguageEntity(final String code, final String title) {
		this.code = code;
		this.title = title;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
