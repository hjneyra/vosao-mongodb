package com.soft2.entity;


public class MessageEntity extends BaseEntityImpl {

	private String code;
	private String languageCode;
	private String value;

	public MessageEntity() {
	}

	public MessageEntity(final String code, final String languageCode, 
			final String value) {
		this.code = code;
		this.languageCode = languageCode;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
