package com.soft2.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.soft2.dao.UserDao;
import com.soft2.dao.UserGroupDao;
import com.soft2.entity.ConfigEntity;
import com.soft2.entity.UserEntity;
import com.soft2.utils.EmailUtil;
import com.soft2.utils.HashUtil;
import com.soft2.utils.StreamUtil;

public class UsrService {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	UserDao userDao;
	
	@Inject
	UserGroupDao userGroupDao;
		
	@Inject
	I18nService i18n;
	
	@Autowired
	VosaoContextService vosaoContextService;
		
	public List<String> validateBeforeUpdate(final UserEntity user) {
		List<String> errors = new ArrayList<String>();
		UserEntity foundUser = userDao.getByEmail(user.getEmail());
		if (user.getId() == null) {
			if (foundUser != null) {
				errors.add(i18n.get("user_already_exists"));
			}
		}
		else {
			if (foundUser != null && !foundUser.getId().equals(user.getId())) {
				errors.add(i18n.get("user_already_exists"));
			}
		}
		if (StringUtils.isEmpty(user.getEmail())) {
			errors.add(i18n.get("email_is_empty"));
		}
		return errors;
	}

	public void remove(List<String> ids) {
		if (VosaoContextService.getInstance().getUser().isAdmin()) {
			userGroupDao.removeByUser(ids);
			userDao.remove(ids);
		}
	}

	public void forgotPassword(String email) {
		UserEntity user = userDao.getByEmail(email);
		if (user == null) {
			return;
		}
		String key = HashUtil.getMD5(email 
				+ String.valueOf((new Date()).getTime()));
		user.setForgotPasswordKey(key);
		userDao.insert(user);
		String template = "";
		try {
			template = StreamUtil.getTextResource(
					"org/vosao/resources/html/forgot-letter.html");
		}
		catch (IOException e) {
			logger.error(e.getMessage());
			return;
		}
		ConfigEntity config = VosaoContextService.getInstance().getConfig();
//		VelocityContext context = new VelocityContext();
//		context.put("user", user);
//		context.put("config", config);
//		context.put("key", key);
//		String letter = vosaoContextService.getSystemService().render(template, context);
//		String error = EmailUtil.sendEmail(letter, "Forgot password", 
//				config.getSiteEmail(), "Site admin", email);
//		if (error != null) {
//			logger.error(error);
//		}
	}	
	
}