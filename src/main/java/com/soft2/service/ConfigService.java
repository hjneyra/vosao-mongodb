package com.soft2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.soft2.entity.ConfigEntity;


@Service
public class ConfigService {
	
	@Inject
	I18nService i18n;
	
	public ConfigEntity getConfig() {
		return VosaoContextService.getInstance().getConfig();
	}

	public boolean isTextFileExt(String ext) {
		ConfigEntity config = getConfig();
		String[] exts = config.getEditExt().split(",");
		for (String textExt : exts) {
			if (ext.equals(textExt)) {
				return true;
			}
		}
		return false;
	}

	public boolean isImageFileExt(String ext) {
		String[] exts = {"jpg","jpeg","png","gif","ico"};
		for (String imageExt : exts) {
			if (ext.equals(imageExt)) {
				return true;
			}
		}
		return false;
	}

	public List<String> validateBeforeUpdate(ConfigEntity entity) {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(entity.getSiteDomain())) {
			errors.add(i18n.get("config.site_domain_is_empty"));
		}
		if (StringUtils.isEmpty(entity.getSiteEmail())) {
			errors.add(i18n.get("config.site_email_is_empty"));
		}
		if (VosaoContextService.getInstance().isSkipUrl(entity.getSiteUserLoginUrl())) {
			errors.add(entity.getSiteUserLoginUrl() 
					+ i18n.get("config.url_reserved"));
		}
		return errors;
	}

}
