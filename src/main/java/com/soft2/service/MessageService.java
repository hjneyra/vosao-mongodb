package com.soft2.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.soft2.entity.MessageEntity;
import com.soft2.dao.MessageDao;


@Service
public class MessageService {

	@Inject
	I18nService i18n;
	
	@Inject
	MessageDao messageDao;
	
	private static final String DEFAULT_BUNDLE_LANGUAGE = "en";
	
	public Map<String, String> getBundle(String languageCode) {
		Map<String, String> result = new HashMap<String, String>();
		addMessages(result, DEFAULT_BUNDLE_LANGUAGE);
		addMessages(result, languageCode);
		return result;
	}

	private void addMessages(Map<String, String> bundle, String language) {
		List<MessageEntity> messages = messageDao.select(
				language);
		for (MessageEntity message : messages) {
			if (!StringUtils.isEmpty(message.getValue())) {
				bundle.put(message.getCode(), message.getValue());
			}
		}
	}
	
	public List<String> validateBeforeUpdate(MessageEntity entity) {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(entity.getCode())) {
			errors.add(i18n.get("code_is_empty"));
		}
		if (StringUtils.isEmpty(entity.getLanguageCode())) {
			errors.add(i18n.get("language_code_is_empty"));
		}
		return errors;
	}
	
	public List<MessageEntity> getAll() {
		return messageDao.getAll();
	}

	public String insert(MessageEntity m) {
		messageDao.save(m);
		return m.getId();
	}

	public void remove(String id) {
		messageDao.removeById(id);
		return;
	}
	
}
