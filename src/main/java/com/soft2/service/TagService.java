package com.soft2.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.soft2.dao.PageTagDao;
import com.soft2.dao.TagDao;
import com.soft2.entity.PageTagEntity;
import com.soft2.entity.TagEntity;
import com.soft2.service.decorator.TreeItemDecorator;

@Service
public class TagService {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	TagDao tagDao;

	@Inject
	PageTagDao pageTagDao;
	
	@Inject
	I18nService i18n;

	private static final Pattern TAG_NAME_DISALLOWED = Pattern.compile("[/ ]");

	public List<TreeItemDecorator<TagEntity>> getTree() {
		List<TreeItemDecorator<TagEntity>> result = new ArrayList<TreeItemDecorator<TagEntity>>();
		List<TagEntity> tags = tagDao.getAll();
		Map<String, TreeItemDecorator<TagEntity>> buf = new HashMap<String, TreeItemDecorator<TagEntity>>();
		for (TagEntity tag : tags) {
			buf.put(tag.getId(), new TreeItemDecorator<TagEntity>(tag, null));
		}
		for (String id : buf.keySet()) {
			TreeItemDecorator<TagEntity> tag = buf.get(id);
			if (tag.getEntity().getParent() == null) {
				result.add(tag);
			} else {
				TreeItemDecorator<TagEntity> parent = buf.get(tag.getEntity()
						.getParent());
				if (parent != null) {
					parent.getChildren().add(tag);
					//tag.setParent(parent);
				}
			}
		}
		return result;
	}

	public String validateBeforeSave(TagEntity tag) {
		if (StringUtils.isEmpty(tag.getTitle())) {
			return i18n.get("title_is_empty");
		}
		if (StringUtils.isEmpty(tag.getName())) {
			return i18n.get("name_is_empty");
		} else {
			TagEntity found = tagDao.getByName(tag.getParent(),
					tag.getName());
			if (tag.isNew()) {
				if (found != null) {
					return i18n.get("tag_already_exists");
				}
			} else {
				if (found != null && !found.getId().equals(tag.getId())) {
					return i18n.get("tag_already_exists");
				}
			}
		}
		if (TAG_NAME_DISALLOWED.matcher(tag.getName()).find()) {
			return i18n.get("tag_wrong_symbol");
		}
		return null;
	}

	public void remove(String id) {
		List<PageTagEntity> pageTags = pageTagDao.getAll();
		for (PageTagEntity pageTag : pageTags) {
			if (pageTag.getTags().contains(id)) {
				pageTag.getTags().remove(id);
				pageTagDao.insert(pageTag);
			}
		}
		removeTree(id);
	}

	private void removeTree(String id) {
		List<TagEntity> children = pageTagDao.selectByParent(id);
		for (TagEntity child : children) {
			removeTree(child.getId());
		}
		tagDao.removeById(id);
	}

	public TreeItemDecorator<TagEntity> getTree(String name) {
		for (TreeItemDecorator<TagEntity> item : getTree()) {
			if (item.getEntity().getName().equals(name)) {
				return item;
			}
		}
		return null;
	}

	public TagEntity getByPath(String tagPath) {
		int start = tagPath.startsWith("/") ? 1 : 0;
		String[] names = tagPath.substring(start).split("/");
		String parent = null;
		TagEntity tag = null;
		for (String name : names) {
			tag = tagDao.getByName(parent, name);
			if (tag == null) {
				return null;
			}
			parent = tag.getId();
		}
		return tag;
	}

	public String getPath(TagEntity tag) {
		String result = tag.getName();
		String parent = tag.getParent();
		while (parent != null) {
			TagEntity parentTag = tagDao.getById(parent);
			if (parentTag != null) {
				result = parentTag.getName() + "/" + result;
				parent = parentTag.getParent();
			} else {
				logger.error("Tag not found " + parent);
				parent = null;
			}
		}
		return "/" + result;
	}

	public void addTag(String pageURL, TagEntity tag) {
		PageTagEntity pageTag = pageTagDao.getByURL(pageURL);
		if (pageTag == null) {
			pageTag = new PageTagEntity(pageURL);
		}
		if (!pageTag.getTags().contains(tag.getId())) {
			pageTag.getTags().add(tag.getId());
		}
		pageTagDao.insert(pageTag);
		if (!tag.getPages().contains(pageURL)) {
			tag.getPages().add(pageURL);
			tagDao.save(tag);
		}
	}

	public void removeTag(String pageURL, TagEntity tag) {
		PageTagEntity pageTag = pageTagDao.getByURL(pageURL);
		if (pageTag != null) {
			if (pageTag.getTags().contains(tag.getId())) {
				pageTag.getTags().remove(tag.getId());
				pageTagDao.insert(pageTag);
			}
		}
		if (tag.getPages().contains(pageURL)) {
			tag.getPages().remove(pageURL);
			tagDao.save(tag);
		}
	}

	public String insert(TagEntity tag) {
		validateBeforeSave(tag);
		tagDao.save(tag);
		return tag.getId();
	}

}
