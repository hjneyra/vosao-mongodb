package com.soft2.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.soft2.entity.ContentPermissionEntity;
import com.soft2.entity.UserEntity;
import com.soft2.entity.GroupEntity;
import com.soft2.entity.UserGroupEntity;
import com.soft2.enums.ContentPermissionType;
import com.soft2.utils.UrlUtil;
import com.soft2.dao.GroupDao;
import com.soft2.dao.UserGroupDao;
import com.soft2.dao.ContentPermissionDao;

@Service
public class ContentPermissionService {

	@Inject
	GroupDao groupDao;
	
	@Inject
	UserGroupDao userGroupDao;
	
	@Inject
	ContentPermissionDao contentPermissionDao;
	
	public ContentPermissionEntity getGuestPermission(final String url) {
		GroupEntity guests = groupDao.getGuestsGroup();
		ContentPermissionEntity result = getGroupPermission(url, guests.getId());
		result.setUrl(url);
		return result;
	}
	
	private ContentPermissionEntity consolidatePermissions(
			List<ContentPermissionEntity> permissions) {
		ContentPermissionEntity result = new ContentPermissionEntity();
		result.setPermission(ContentPermissionType.DENIED);
		result.setAllLanguages(false);
		Set<String> languages = new HashSet<String>();
		for (ContentPermissionEntity perm : permissions) {
			if (perm.isMyPermissionHigher(result)) {
				result.setPermission(perm.getPermission());
				if (perm.isAllLanguages()) {
					result.setAllLanguages(true);
				}
				languages.addAll(perm.getLanguagesList());
			}
		}
		String langs = "";
		for (String lang : languages) {
			langs += (langs.equals("") ? "" : ",") + lang;
		}
		result.setLanguages(langs);
		return result;
	}
	
	public ContentPermissionEntity getPermission(final String url,
			final UserEntity user) {
		if (user == null) {
			return getGuestPermission(url);
		}
		if (user.isAdmin()) {
			return new ContentPermissionEntity(url, ContentPermissionType.ADMIN);
		}
		List<UserGroupEntity> userGroups = userGroupDao.selectByUser(user.getId());
		userGroups.add(new UserGroupEntity(groupDao.getGuestsGroup().getId(), user.getId()));
		List<ContentPermissionEntity> permissions = new ArrayList<ContentPermissionEntity>();
		for (UserGroupEntity userGroup : userGroups) {
			ContentPermissionEntity contentPermission = getGroupPermission(url,
					userGroup.getGroupId());
			if (contentPermission != null) {
				permissions.add(contentPermission);
			}
		}
		ContentPermissionEntity result = consolidatePermissions(permissions);
		result.setUrl(url);
		return result;
	}
	
	private ContentPermissionEntity getGroupPermission(String url, String groupId) {
		String myUrl = url;
		while (myUrl != null) {
			ContentPermissionEntity perm = contentPermissionDao.getByUrlGroup(myUrl, groupId);
			if (perm != null) {
				return perm;
			}
			if (myUrl.equals("/")) {
				myUrl = null;
			} else {
				myUrl = UrlUtil.getParentFriendlyURL(myUrl);
			}
		}
		return null;
	}

	public List<ContentPermissionEntity> getAll() {
		return contentPermissionDao.getAll();
	}

	public String insert(ContentPermissionEntity cont) {
		contentPermissionDao.save(cont);
		return cont.getId();
	}

	public void remove(String id) {
		contentPermissionDao.removeById(id);
		return;
	}

	// TODO: missing methods
}
