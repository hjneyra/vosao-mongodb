package com.soft2.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.soft2.dao.CommentDao;
import com.soft2.entity.CommentEntity;
import com.soft2.entity.ConfigEntity;
import com.soft2.service.ContentPermissionService;
import com.soft2.service.SystemService;
import com.soft2.utils.EmailUtil;
import com.soft2.utils.StrUtil;
import com.soft2.entity.ContentPermissionEntity;
import com.soft2.entity.PageEntity;

@Service
public class CommentService {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final String COMMENT_LETTER_SUBJECT = "New comment";
	
	@Inject
	CommentDao commentDao;
	
	private ContentPermissionService contentPermissionBusiness;
	private SystemService systemService;
	
	public CommentEntity addComment(String name, String content, 
			PageEntity page) {

		ConfigEntity config = VosaoContextService.getInstance().getConfig();
		CommentEntity comment = new CommentEntity(name, content, 
				new Date(), page.getFriendlyURL());
		commentDao.save(comment);
		getSystemService().getPageCache().remove(
			page.getFriendlyURL()
		);
		List<String> toAddresses = StrUtil.fromCSV(config.getCommentsEmail());
		if (toAddresses.size() == 0) {
			toAddresses.add(config.getSiteEmail());
		}
		for (String email : toAddresses) {
			EmailUtil.sendEmail(createCommentLetter(comment, page), 
				COMMENT_LETTER_SUBJECT, 
				config.getSiteEmail(), 
				config.getSiteDomain() + " admin", 
				StringUtils.trimWhitespace(email));
			logger.debug("New comment letter was sent to " + email);
		}
		return comment;
	}
	
	private String createCommentLetter(CommentEntity comment, PageEntity page) {
		ConfigEntity config = VosaoContextService.getInstance().getConfig();
		StringBuffer b = new StringBuffer();
		b.append("<p>New comment was added to page ")
		    .append(config.getSiteDomain()).append(page.getFriendlyURL())
		    .append(" by ").append(comment.getName()).append("</p>")
		    .append(comment.getContent());
		return b.toString();
	}

	private boolean isChangeGranted(List<String> ids) {
		if (ids.size() > 0) {
			CommentEntity comment = commentDao.getById(ids.get(0));
			ContentPermissionEntity permission = getContentPermissionBusiness()
					.getPermission(	comment.getPageUrl(), 
							VosaoContextService.getInstance().getUser());
			if (permission != null) {
				return permission.isChangeGranted();
			}
		}		
		return false;
	}
	
	public void disable(List<String> ids) {
		clearPageCache(ids);
		if (isChangeGranted(ids)) {
			commentDao.disable(ids);
		}		
	}

	public void enable(List<String> ids) {
		clearPageCache(ids);
		if (isChangeGranted(ids)) {
			commentDao.enable(ids);
		}		
	}

	public void remove(List<String> ids) {
		clearPageCache(ids);
		if (isChangeGranted(ids)) {
			commentDao.remove(ids);
		}		
	}

	private void clearPageCache(List<String> commentIds) {
		for(String id : commentIds) {
			CommentEntity comment = commentDao.getById(id);
			if (comment != null) {
				getSystemService().getPageCache().remove(
					comment.getPageUrl()
				);
			}
		}
	}

	private ContentPermissionService getContentPermissionBusiness() {
		if (contentPermissionBusiness == null) {
			contentPermissionBusiness = new ContentPermissionService();
		}
		return contentPermissionBusiness;
	}
	
	public SystemService getSystemService() {
		if (systemService == null) {
			systemService = new SystemService();
		}
		return systemService;
	}

	public List<CommentEntity> getAll() {
		return commentDao.getAll();
	}

	public String insert(CommentEntity m) {
		commentDao.save(m);
		return m.getId();
	}

	public void remove(String id) {
		commentDao.removeById(id);
		return;
	}
}
