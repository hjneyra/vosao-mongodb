package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.MessageEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class MessageDaoTest {
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	MessageDao messageDao;
	
	List<MessageEntity> messages = new ArrayList<MessageEntity>();

	private MessageEntity addMessage(String code, String languageCode,
			String value) {
		MessageEntity obj = new MessageEntity(code,	languageCode, value);
		messages.add(obj);
		return messageDao.save(obj);
	}
	
	@Before
	public void setupData() {
		addMessage("test", "en", "test_en");
		addMessage("test", "ru", "test_ru");
		addMessage("test", "uk", "test_uk");
		addMessage("test2", "en", "test2_en");
		addMessage("test2", "ru", "test2_ru");
		addMessage("test2", "uk", "test2_uk");	
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (MessageEntity message : messages) {
			messageDao.remove(message);
		}
	}

	@Test
	public void testSave() {
		MessageEntity message = new MessageEntity();
		messageDao.save(message);
		logger.debug("Persist Message ID: " + message.getId());
		Assert.assertNotNull(message.getId());
		messages.add(message);
	}

	@Test
	public void testSelectByCode() {
		List<MessageEntity> list = messageDao.selectByCode(
				"test");
		Assert.assertEquals(3, list.size());
		list = messageDao.selectByCode(null);
		Assert.assertEquals(0, list.size());
		list = messageDao.selectByCode("null");
		Assert.assertEquals(0, list.size());
	}

	@Test
	public void testGetByCode() {
		MessageEntity m = messageDao.getByCode("test", "en");
		Assert.assertNotNull(m);
		Assert.assertEquals("test_en", m.getValue());
		m = messageDao.getByCode("test3", "en");
		Assert.assertNull(m);
		m = messageDao.getByCode(null, null);
		Assert.assertNull(m);
		m = messageDao.getByCode("test", null);
		Assert.assertNull(m);
		m = messageDao.getByCode(null, "en");
		Assert.assertNull(m);
	}

	@Test
	public void testSelect() {
		List<MessageEntity> list = messageDao.select("en");
		Assert.assertEquals(2, list.size());
		list = messageDao.select("ru");
		Assert.assertEquals(2, list.size());
		String lang = null;
		list = messageDao.select(lang);
		Assert.assertEquals(0, list.size());
		list = messageDao.select("fr");
		Assert.assertEquals(0, list.size());
	}
	
	@Test
	public void testGetCollectionClass() {
		Assert.assertSame(MessageEntity.class, messageDao.getCollectionClass());
	}
}
