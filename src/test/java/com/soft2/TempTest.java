package com.soft2;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import org.junit.Test;

import com.soft2.entity.MessageEntity;

public class TempTest {
	@Test
	public void test1() {
		Binding binding = new Binding();
		MessageEntity m = new MessageEntity("code123", "ES", "value123");
		binding.setVariable("msg", m);
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		Object value = shell.evaluate("println 'Hello World! in '; println msg.code; x = 123; return foo * 10");
		assert value.equals(new Integer(20));
		assert binding.getVariable("x").equals(new Integer(123));
	}
}
