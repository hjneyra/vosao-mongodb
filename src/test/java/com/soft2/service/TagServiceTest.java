package com.soft2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.dao.TagDao;
import com.soft2.entity.TagEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class TagServiceTest {
	
	@Inject
	TagDao tagDao;
	
	@Inject
	TagService tagService;
	
	List<TagEntity> tags = new ArrayList<TagEntity>();
	
	private TagEntity addTag(String parent, String name){
	    TagEntity ent = new TagEntity(parent, name, name);
	    tags.add(ent);
	    return tagDao.save(ent);
	}
	
	
	@Before
	public void setupData() {
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (TagEntity obj : tags) {
			tagDao.remove(obj);
		}
	}
	
	@Test
	public void testGetByPath() {
		TagEntity t1 = addTag(null, "t1");
		TagEntity t2 = addTag(null, "t2");
		TagEntity t3 = addTag(t1.getId(), "t3");
		TagEntity t4 = addTag(t3.getId(), "t4");
		TagEntity t = tagService.getByPath("/t1");
		Assert.assertNotNull(t);
		Assert.assertEquals("t1", t.getName());
		t = tagService.getByPath("/t2");
		Assert.assertNotNull(t);
		Assert.assertEquals("t2", t.getName());
		t = tagService.getByPath("/t1/t3");
		Assert.assertNotNull(t);
		Assert.assertEquals("t3", t.getName());
		t = tagService.getByPath("/t1/t3/t4");
		Assert.assertNotNull(t);
		Assert.assertEquals("t4", t.getName());
	}
	
	@Test
	public void testGetPath() {
		TagEntity t1 = addTag(null, "t1");
		TagEntity t2 = addTag(null, "t2");
		TagEntity t3 = addTag(t1.getId(), "t3");
		TagEntity t4 = addTag(t3.getId(), "t4");
		String p = tagService.getPath(t4);
		Assert.assertEquals("/t1/t3/t4", p);
		p = tagService.getPath(t2);
		Assert.assertEquals("/t2", p);
		p = tagService.getPath(t3);
		Assert.assertEquals("/t1/t3", p);
	}

}
